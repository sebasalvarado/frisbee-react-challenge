# Frisbee React Challenge

Te presentamos nuestra evaluación para candidatos como Front-end (React) Engineers en [Frisbee App](frisbee.com.ec).

Quién no ama Elon Musk y su empresa SpaceX? Creamos este challenge para que podamos aprender un poco más de la empresa, sus logros y que nos puedas mostrar tus habilidades con React!


## De qué se trata esta evaluación?

Queremos darte el tiempo y la libertad de mostrarnos tus skills con un proyecto real. No creemos que la mejor forma de evaluar a un candidato es mediante una entrevista con presión de tiempo, temas desconocidos o rompecabezas matemáticos que no son relevantes para un/a miembro/a del Equipo de Ingenieria en Frisbee.

Diseñamos esta evaluación con la intención de hacer una evaluación real y poder discutir tu solución e implementación más a fondo en nuestra siguiente entrevista.

Creemos que el desafío no debería demorarte más de un día para completar, asi que utiliza eso como una métrica de tu tiempo.

El proyecto inicial tiene la estructura de carpetas como consideramos que es una buena práctica de tener las aplicaciones, te recomendamos mantener esa estructura para que el proceso de evaluación sea más efectivo.


Esperamos la disfrutes!

## Tecnologías:
- TypeScript y Apollo Client (GraphQL)
- App creada con `create-react-app`
- SpaceX GraphQL API



## Como correr el proyecto

Debes tener Node.js y NPM instalado localmente.

```bash
$ git clone git@gitlab.com:sebasalvarado/frisbee-react-challenge.git
$ cd frisbee-react-challenge
$ npm install
$ npm start
```

El proyecto inicial cuenta con una sidebar de navegación sencillo en la parte izquierda con una lista de todos los `launch` de SpaceX desde sus inicios. Tu trabajo va a ser mejorar el diseño de esta aplicación y darnos más información de `Misiones` y `Cohetes` de SpaceX.


## Desafío

Vamos a utlizar la API en GraphQL de [`SpaceX`](https://spacexdata.herokuapp.com/graphql). Es un servicio sencillo de utilizar pero muy poderoso para recibir información diferentes ámbitos de SpaceX.


## Funcionalidad

## Tarea #1: Mejorar Navegación

Vamos a utilizar los componentes de [Material UI](https://material-ui.com/getting-started/installation/) para mejorar la navegación de la aplicación inicial.

- Utilizar los componentes `List`, `ListItem` y cualquier otro componente que creas relevante para implementar el Sidebar donde están todos los `Launch` de SpaceX.

- Mostrar claramente cuando un ítem de la lista está seleccionado. Por ejemplo: Color diferente, etc

- Mostrar claramente en cada ítem de la lista cuando un `launch` fue fallido o exitoso con texto de color rojo y verde respectivamente.

- Implementar toda la lista de `Launch` bajo un [`Nested List`](https://material-ui.com/components/lists/#nested-list) para poder esconder y mostrar la lista de todos los `Launch`.

## Tarea #2: SpaceX Missions

GraphQL Query: `missions`

- Agregar una nueva [`Nested List`](https://material-ui.com/components/lists/#nested-list) para mostrar una lista de todas las misiones de SpaceX.

- Al darle clic a cada `ListItem` de una misión debemos navegar y poder ver más detalles de la misión. Te invitamos a que diseñes esa interfaz de una manera en que la información se vea claramente y fácil de digerir. Por lo menos debes mostrar el: nombre, ID, website y twitter de la misión. Pero si quieres agregar más puntos de información puedes hacerlo!

- [Bonus] Puedes mostrar una lista de todos los `payloads` de la misión después de consultar el `Payload` por el ID que está en la lista en el campo `payload_id`.

## Tarea #3: SpaceX Rockets

GraphQL Query: `rockets`

- Agregar una nueva [`Nested List`](https://material-ui.com/components/lists/#nested-list) para mostrar una lista de todas las cohetes de SpaceX.

- Al darle clic a cada `ListItem` de una misión debemos navegar y poder ver más detalles del cohete. Te invitamos a que diseñes esa interfaz de una manera en que la información se vea claramente y fácil de digerir. Por lo menos debes mostrar los siguientes campos: `active`, boosters, cost_per_launch, first_flight, countries, mass y diameter.

- [Bonus] Puedes mostrar un carrusel de fotos con las imágenes de `flickr_images`.


### Componentes UI

Vamos a utilizar los componentes de `Material UI`

- Documentación: https://material-ui.com/getting-started/installation/

Te invitamos a que leas la documentación, lo instales en el proyecto y utilices estos componentes en tu solución.


### GraphQL
El servicio de `SpaceX` lo vamos a tener del endpoint:

- GraphQL: https://spacexdata.herokuapp.com/graphql


Si no estás tan familarizado/a con GraphQL te invitamos a que leas más en su documentación oficial, así como Apollo Client:

- GraphQL Documentación: https://graphql.org/learn/
- Apollo Client: https://www.apollographql.com/docs/tutorial/introduction/




## Evaluación
- 30% - UI y UX
- 40% - Diseño General y Estructura
- 30% - Data Management y Algoritmos



## Cómo Enviar tu Solución:

Crea una branch en tu proyecto local con el siguiente formato `nombre-apellido`, después de revisarlo juntos en la siguiente entrevista vas a poder subirlo al repositorio remoto. 
